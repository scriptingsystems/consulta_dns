# consulta_dns

## Getting Started

Fuente: https://www.ibm.com/docs/es/capm?topic=monitors-dns-monitor

NS: Servidor de nombre de autorización. Lista de servidores DNS autorizados para ese dominio

```
$ python -m virtualenv -p /Library/Frameworks/Python.framework/Versions/2.7/bin/python consulta_dns
$ cd consulta_dns
$ source bin/activate
$ pip install dnspython
```
